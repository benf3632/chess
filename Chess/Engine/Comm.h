#pragma once
#include <iostream>
#include <string>
#include "Piece.h"
#include "King.h"

class Comm
{
public:
	
	Comm();
	virtual ~Comm();
	std::string createCharOpcode(const std::string str, const int pos, Piece*** board);
	std::string analizeStr(std::string, Piece*** board);
private:
	
	int strToPos(std::string);
	std::string _opcode;
	void movePiece(Piece*** board, int pos, bool temp);

	char _turn;
	int _blackKing;
	int _whiteKing;
	Piece* _tempPiece;
};

