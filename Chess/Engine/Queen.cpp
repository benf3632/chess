#include "Queen.h"



Queen::Queen(char color, char type) : Piece(color, type)
{
}


Queen::~Queen()
{
}

int Queen::checkMove(int pos, Piece*** board)
{
	int src = pos / HUNDRED;
	int result = 0;

	Rook* r = nullptr; 
	Bishop* b = nullptr;

	r = r->operator=(this); //we create a pointer of type rook because the queen can move like rook
	b = b->operator=(this); //we create a pointer of type bishop because the queen ca also move like bishop

	result = r->checkMove(pos, board); //checks if it can move like rook

	if (result) //if cant move like rook check the other option
	{
		result = b->checkMove(pos, board); //checks if it can move like bishop 
	}

	//frees the memory
	delete r; 
	delete b;


	return result;
}