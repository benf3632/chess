#pragma once
#include "Piece.h"
#define RUU 12
#define RRU 21
#define RRD 19
#define RDD 8
#define LUU -8
#define LLU -19
#define LLD -21
#define LDD -12
class Knight : public Piece
{
public:
	Knight(char color, char type);
	virtual ~Knight();
	virtual int checkMove(int pos, Piece*** board);
};

