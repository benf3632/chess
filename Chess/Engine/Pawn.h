#pragma once
#include "Piece.h"
class Pawn : public Piece
{
public:
	Pawn(char color, char type);
	virtual ~Pawn();
	virtual int checkMove(int pos, Piece*** board);
};

