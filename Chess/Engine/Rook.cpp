#include "Rook.h"



Rook::Rook(char color, char type): Piece(color, type)
{
}


Rook::~Rook()
{
}

int Rook::checkMove(int pos, Piece*** board)
{
	int src = pos / HUNDRED;
	int des = pos % HUNDRED;
	bool row = false;
	bool line = false;


	if (src / TEN == des / TEN)
	{
		row = true; //checks if the move is on row
	}
	else if (src % TEN == des % TEN)
	{
		line = true; // checks if the move is on line
	}

	if (!row && !line) //if not on line and not on row --> illegal movment
	{
		return 6;
	}
	else if (row) //if its movment on row
	{
		if (des % TEN > src % TEN) //checks if goes up
		{
			//checks all up locations
			for (int i = src % TEN; i < des % TEN -1; i++)
			{
				if (board[i][src / TEN - 1]) //if one of the locations is not nullptr that means there is a piece and the rook cant move
				{
					return 6;
				}
			}
		}
		else
		{
			//checks all the locations downwords
			for (int i = src % TEN - 2; i > des % TEN - 1; i--)
			{
				if (board[i][src / TEN - 1]) //if one of the loc is not nullptr the rook cant move
				{
					return 6;
				}
			}
		}
	}
	else if (line) //if the movment is on the line
	{
		if (des / TEN > src / TEN) //checks if its movment to the right 
		{
			for (int i = src / TEN; i < des / TEN - 1 ; i++) //checks all locations to the right
			{
				if (board[src % TEN - 1][i]) //if one is nullptr the rook cant move
				{
					return 6;
				}
			}
		}
		else
		{
			for (int i = src / TEN - 2; i > des / TEN; i--) //checks all loc to the left
			{
				if (board[src % TEN - 1][i]) //if one is nullptr the rook cant move
				{
					return 6;
				}
			}
		}
	}
	
	return 0;
}

/*
To create temorary rook object
*/
Rook* Rook::operator=(Piece* piece)
{
	Rook* temp = new Rook(piece->getColor(), piece->getType());
	return temp;
}