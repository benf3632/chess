#include "Knight.h"


Knight::Knight(char color, char type) : Piece(color, type)
{
}

Knight::~Knight()
{
}

/*knight can move over other pieces
It moves to a square that is two squares away horizontally and one square vertically,
or two squares vertically and one square horizontally*/
int Knight::checkMove(int pos, Piece*** board)
{
	int result = 0;
	int des = pos % HUNDRED;
	int src = pos / HUNDRED;
	//            |
	//       -8 * | *+12
	//   - 19 *   |    *+21
	//	 -------piece-------
	//	          |
	//    -21 *   |   *+19
	//     - 12 * | *+8

	//check if piece des is not one of the 8 options posible
	if (src + RRU != des && src + RUU != des && src + LLD != des && src + LDD != des &&
		src + LLU != des && src + LUU != des && src + RRD != des && src + RDD != des)
	{
		result = 6;
	}
	return result;
}