#pragma once
#include "Piece.h"

class Rook : public Piece
{
public:
	Rook(char color, char type);
	virtual ~Rook();
	virtual int checkMove(int pos, Piece*** board);
	Rook* operator=(Piece* piece);
};

