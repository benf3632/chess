#include "Piece.h"

Piece::Piece(char color, char type) : _color(color), _type(type)
{

}

Piece::Piece()
{

}

Piece::~Piece()
{
}

int Piece::checkDesSrc(int pos,Piece*** board, char turn) const
{
	int result = 0;
	int des = pos % HUNDRED;
	int src = pos / HUNDRED;
	if (src == des) //check if des and src aren't the same place
	{
		result = 7;
	}

	else if (board[src % TEN - 1][src / TEN - 1] == nullptr) //check if src is empty 

	{
		result = 2;
	}
	//check if in src the piece is of the correct player
	else if (board[src % TEN - 1][src / TEN - 1] != nullptr && board[src % TEN - 1][src / TEN - 1]->getColor() != turn)

	{
		result = 2;
	}
	//check if in des there is no piece that belongs to the current player
	else if (board[des % TEN - 1][des / TEN - 1] != nullptr && board[des % TEN - 1][des / TEN - 1]->getColor() == turn)
	{
		result = 3;
	}
	//check if src and des are on the board
	else if (src < START_OF_BOARD || src > END_OF_BOARD || des < START_OF_BOARD || des > END_OF_BOARD) 
	{
		result = 5;
	}
	return result;
}

/*returns color of piece*/
char Piece::getColor() const
{
	return _color;
}

/*returns type of piece*/
char Piece::getType() const
{
	return _type;
}