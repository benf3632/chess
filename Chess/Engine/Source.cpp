#include "Pipe.h"
#include <iostream>
#include <thread>
#include <windows.h>
#include <string.h>
#include <stdio.h>
#include "Comm.h"
#include "Piece.h"
#include "Bishop.h"
#include "Rook.h"
#include "Pawn.h"
#include "Knight.h"
#include "Queen.h"
#include "King.h"

using namespace std;

void initBoard(Piece*** board);
void freeBoard(Piece*** board);


int main(void)
{
	srand(time_t(NULL));

	Piece*** board = new Piece**[8];
	for (int i = 0; i < 8; i++)
	{
		board[i] = new Piece*[8];
	}
	initBoard(board);

	//start chessGraphic.exe
	char temp[1000];
	GetCurrentDirectoryA(1000, temp);
	char buffer[1000];
	sprintf(buffer, "start %s\\chessgraphics.exe", temp);
	cout << buffer << endl;
	system(buffer);
	Sleep(500);

	Pipe p;
	bool isConnect = p.connect();
	
	string ans;


	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return 0;
		}
	}
	

	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE

	strcpy(msgToGraphics, "RNBQKBNRPPPPPPPP################################pppppppprnbqkbnr0"); 
	
	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();
	
	
	Comm comm;

	while (msgFromGraphics != "quit")
	{
		strcpy(msgToGraphics, comm.analizeStr(msgFromGraphics, board).c_str());

		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);   

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}

	p.close();

	freeBoard(board);

	system("pause");
	return 0;
}

void initBoard(Piece*** board)
{ 
	board[0][0] = new Rook('1', 'r');
	board[0][1] = new Knight('1', 'n');
	board[0][2] = new Bishop('1', 'b');
	board[0][3] = new Queen('1', 'q');
	board[0][4] = new King('1', 'k');
	board[0][5] = new Bishop('1', 'b');
	board[0][6] = new Knight('1', 'n');
	board[0][7] = new Rook('1', 'r');

	for (int i = 0; i < 8; i++)
	{
		board[1][i] = new Pawn('1', 'p');
	}


	for (int i = 2; i < 6; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			board[i][j] = nullptr;
		}
	}



	board[7][0] = new Rook(0, 'R');
	board[7][1] = new Knight(0, 'N');
	board[7][2] = new Bishop(0, 'B');
	board[7][3] = new Queen(0, 'Q');
	board[7][4] = new King(0, 'K');
	board[7][5] = new Bishop(0, 'B');
	board[7][6] = new Knight(0, 'N');
	board[7][7] = new Rook(0, 'R');

	

	for (int i = 0; i < 8; i++)
	{
		board[6][i] = new Pawn(0, 'P');
	}
}

void freeBoard(Piece*** board)
{
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{

			if (board[i][j])
			{
				delete board[i][j];
				board[i][j] = nullptr;
				
			}
		}
		delete[] board[i];
	}

	delete[] board;
}