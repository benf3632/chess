#pragma once
#include "Piece.h"
#include "Bishop.h"
#include "Rook.h"
class Queen :
	public Piece
{
public:
	Queen(char color, char type);
	virtual ~Queen();

	virtual int checkMove(int pos, Piece*** board);
};

