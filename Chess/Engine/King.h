#pragma once
#include "Piece.h"
#include <math.h>

class King :
	public Piece
{
public:
	King(char, char);
	virtual ~King();
	virtual int checkMove(int pos, Piece*** board);
	int hasCheck(int pos, Piece*** board, char color, char turn);
	King* operator=(Piece*);
};

