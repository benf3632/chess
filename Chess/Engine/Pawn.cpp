#include "Pawn.h"

Pawn::Pawn(char color, char type) : Piece(color, type)
{

}

Pawn::~Pawn()
{

}

int Pawn::checkMove(int pos, Piece*** board)
{
	int result = 0;
	int des = pos % HUNDRED, src = pos / HUNDRED;
	//check if current player is black or white
	if (this->_color != 0)
	{
		if (src % TEN != 2 && des % TEN > src % TEN + 1 && src % TEN > des % TEN) //checks that player moves only one place forward and not back
		{
			result = 6;
		}
		else if (board[des % TEN - 1][des / TEN - 1] != nullptr) //checks if des nor empty(in case player moves diagonally
		{
			if (src + DIAG_NW != des && src + DIAG_NE != des) //checks if player moved diagonally only ones and forward
			{
				result = 6;
			}

		}
		else if (des / TEN != src / TEN) //checks that player moves only in it's lane
		{
			result = 6;
		}
		//Pawn can move 2 places in beginning of the game and one place in the rest of it in case the way is empty
		else if (src % TEN == 2 && des <= src + 2 || (src +1 == des && board[des % TEN - 1][des / TEN - 1] == nullptr)) 
		{
			result = 0;
		}
		//checks that pawn not moves diagonally
		else if (board[des % TEN - 1][des / TEN - 1] == nullptr && (src - 9 >= des || src + 11 >= des
			|| src + 9 >= des || src - 11 >= des))
		{
			result = 6;
		}
		
	}
	else if (this->_color == 0 ) //in case color is white
	{
		if (src % TEN != 7 && des % TEN  < src % TEN - 1 && src % TEN < des % TEN) //checks that player moves only one place back on board and not forward
		{
			result = 6;
		}
		else if (board[des % TEN - 1][des / TEN - 1] != nullptr) //checks if des nor empty(in case player moves diagonally
		{
			if (src + DIAG_SE != des && src + DIAG_SW != des) //checks if player moved diagonally only ones and back
			{
				result = 6;
			}

		}
		else if (des / TEN != src / TEN) //checks that player moves only in it's lane
		{
			result = 6;
		}
		//Pawn can move 2 places in beginning of the game and one place in the rest of it in case the way is empty
		else if (src % TEN == 7 && des >= src - 2 || src -1 == des && board[des % TEN - 1][des / TEN - 1] == nullptr)
		{
			result = 0;
		}
		//checks that pawn not moves diagonally
		else if (board[des % TEN - 1][des / TEN - 1] == nullptr && (src + DIAG_SE <= des || src + DIAG_SW <= des 
			|| src + DIAG_NE <= des || src + DIAG_NW <= des ))
		{
			result = 6;
		}
		
	}
	
	return result;
}

