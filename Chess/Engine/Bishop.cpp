#include "Bishop.h"



Bishop::Bishop(char color, char type) : Piece(color, type)
{

}


Bishop::~Bishop()
{
}

/*bishop can only move in diagonally*/
int Bishop::checkMove(int pos, Piece*** board)
{
	int result = 0;
	int des = pos % HUNDRED;
	int src = pos / HUNDRED;
	int check = src, add =0;
	if (abs(src % TEN - des % TEN) == abs(src / TEN - des / TEN)) //in diagonally x and y change by the same number
	{
		
		if (des - src < 0) //if des smaller than src than piece moves left    
						                     //         |            
											 //	 -9 	|   +11
											//			|
										    //	-------piece-------
											//			|
								     		//	-11     |   +9
											//			|          
			                                                                                                                                   
		{                                
			if ((des - src) % 9 == 0) //if src changed by a multiple of 9 than piece moves up
			{
				add = DIAG_NW;
			}
			else if((des - src) % 11 == 0) //if src changed by a multiple of 11 than piece moves down
			{
				add = DIAG_SE;
			}
		}
		else //if des bigger than src than piece moves right
		{
			if ((des - src) % 9 == 0) //if src changed by a multiple of 9 than piece moves down
			{
				add = DIAG_SW;
			}
			else if ((des - src) % 11 == 0) ////if src changed by a multiple of 11 than piece moves up
			{
				add = DIAG_NE;
			}
		}
		check += add; //next place in diagonally way
		while (check != des && result == 0) //check all places until des
		{
			
			if (board[check % TEN - 1][check / TEN - 1] != nullptr) //check if way is clear(if every place in the way is empty)
			{
				result = 6;
			}
			check += add;
		}
		
		
		
	}
	else 
	{
		result = 6;
	}
	
	return result;
}

Bishop* Bishop::operator=(Piece* piece)
{
	Bishop* temp = new Bishop(piece->getColor(), piece->getType());
	return temp;
}