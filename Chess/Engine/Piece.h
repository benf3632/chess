#pragma once
#define TEN 10
#define START_OF_BOARD 11
#define DIAG_NE 11
#define DIAG_SE -11
#define DIAG_NW -9
#define DIAG_SW 9
#define HUNDRED 100
#define END_OF_BOARD 88



class Piece {
protected:
	char _color;
	char _type;
	
public:
	Piece();
	Piece(char color, char type);
	virtual ~Piece();

	virtual int checkMove(int pos, Piece*** board) = 0;
	int checkDesSrc(int pos, Piece*** board, char turn) const;
	char getColor() const;
	char getType() const;
};

	
	

