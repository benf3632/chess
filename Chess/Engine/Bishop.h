#pragma once
#include "Piece.h"
#include <math.h>


class Bishop : public Piece
{
public:
	Bishop(char color, char type);
	virtual ~Bishop();
	virtual int checkMove(int pos, Piece*** board);
	Bishop* operator=(Piece* piece);
};

