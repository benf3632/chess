#include "King.h"



King::King(char color, char type) : Piece(color, type)
{
}


King::~King()
{
}

int King::checkMove(int pos, Piece*** board)
{
	int src = pos / HUNDRED;
	int des = pos % HUNDRED;

	//the king can move diagonly one step and up down one step. by calculation of the src and des we can check if one of the moves happend
	//* * * * * * * *
	//* * * * * * * *
	//* * * * * * * *
	//* * 11 10 11 * * *
	//* * 1 K 1 * * *
	//* * 9 10 9 * * *
	//* * * * * * * *
	//* * * * * * * *
	if (abs(des - src) != DIAG_NE && abs(des - src) != 1 && abs(des - src) != DIAG_SW && abs(des - src) != TEN) 
	{
		return 6;
	}


	return 0;
}

int King::hasCheck(int pos, Piece*** board, char color, char turn)
{
	int srcDes = 0;
	int result = 1;

	//goes through every piece in the board
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			srcDes = ((j * TEN + i) + START_OF_BOARD )* HUNDRED + pos; //takes the current pos of the piece adds it to curren pos of the king to create Like move pos
			if (board[i][j] != nullptr)
			{
				if (board[i][j]->getColor() != color) //to skip the same color the check is on
				{
					result = board[i][j]->checkDesSrc(srcDes, board, turn); //checks genral move
					if (!result)
					{
						result = board[i][j]->checkMove(srcDes, board); //checks specific move
					}
				}
			}
			if (!result) //if the result is 0 that means can go to kings pos so it has check
			{
				return 1; //opcode for check
			}
		}
	}
	return 0;
}

/*
To Create temporary object of type king
*/
King* King::operator=(Piece* piece)
{
	King* temp = nullptr;
	temp = new King(piece->getColor(), piece->getType());
	return temp;
}