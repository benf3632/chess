#include "Comm.h"

using namespace std;


Comm::Comm() : _turn(0), _blackKing(51), _whiteKing(58), _tempPiece(nullptr)
{
}


Comm::~Comm()
{
}

/*
Function that moves a piece on the board
*/
void Comm::movePiece(Piece*** board, int pos, bool temp)
{
	int src = pos / HUNDRED;
	int des = pos % HUNDRED;

	//checks if the piece that will be moved is a king to update thiers position
	if (board[src % TEN - 1][src / TEN - 1]->getType() == 'K')
	{
		this->_whiteKing = des;
	}
	else if (board[src % TEN - 1][src / TEN - 1]->getType() == 'k')
	{
		this->_blackKing = des;
	}

	//checks if in the destenation has a piece
	if (board[des % TEN - 1][des / TEN - 1])
	{
		if (temp) // checks if the move is temporarly
		{
			_tempPiece = board[des % TEN - 1][des / TEN - 1]; //stores the piece temporarly
		}
		else
		{
			delete board[des % TEN - 1][des / TEN - 1]; //if the move is permenent delete the piece in the destenation
			board[des % TEN - 1][des / TEN - 1] = nullptr;
		}
	}

	board[des % TEN - 1][des / TEN - 1] = board[src % TEN - 1][src / TEN - 1]; //move the piece

	if (_tempPiece && !temp) //if the move was temporary and a piece was stored
	{
		board[src % TEN - 1][src / TEN - 1] = _tempPiece; //restore the piece
		_tempPiece = nullptr; 
	}
	else
	{
		board[src % TEN - 1][src / TEN - 1] = nullptr; //if the move is permenent set source as nullptr
	}


}

/*
Function that "analize string" (just turns string to int)
*/
string Comm::analizeStr(string msg, Piece*** board)
{
	int pos = this->strToPos(msg);

	return createCharOpcode(msg, pos, board);
}

/*
The main communication with the pipe(sends the pos to check if the move is valid and than returns the correct op code)
*/
string Comm::createCharOpcode(const string str, const int pos, Piece*** board)
{
	int src = pos / HUNDRED;
	

	int opcode = board[src % TEN - 1][src / TEN - 1]->checkDesSrc(pos, board, _turn); //checks genral valid movment of the piece

	if (opcode == 0) //if the general move is valid 
	{
		opcode = board[src % TEN - 1][(src / TEN) - 1]->checkMove(pos, board); //checks if the piece can go to the destenation according to his rules

	}
	
	//if the move is valid and the piece can move to the destenation
	if (opcode == 0)
	{
		movePiece(board, pos, true); //move temporarly the piece


		King* k = nullptr;
		if (_turn == 0) //if the turn is for white
		{
			//checks if the moved piece exposes the white king for check or does not eliminate it from check
			k = k->operator=(board[_whiteKing % TEN - 1][_whiteKing / TEN - 1]);
			opcode = k->hasCheck(_whiteKing, board, _turn, '1');

			
			if (opcode == 1) //if it is sets op code 4
			{
				opcode = 4;
			}
			
			//if not exposes
			if (opcode != 4)
			{
				this->_turn = '1'; //sets the turn for the black
				delete k;
				
				//checks if the moved piece does a check on the black king
				k = k->operator=(board[_blackKing % TEN - 1][_blackKing / TEN - 1]);
				opcode = k->hasCheck(_blackKing, board, _turn, 0);
			}
		}
		else //if the turn is for black
		{	
			//checks if the moved piece is exposes the black king for a check or does not eliminate it from check
			k = k->operator=(board[_blackKing % TEN - 1][_blackKing / TEN - 1]);
			opcode = k->hasCheck(_blackKing, board, _turn, 0);

			if (opcode == 1) //if it is sets the opcode 4
			{
				opcode = 4;
			}

			//if not exposed or eliminated the check
			if (opcode != 4)
			{
				this->_turn = 0; //sets the turn for the white
				delete k;
				//checks if the move made a check on the white king
				k = k->operator=(board[_whiteKing % TEN - 1][_whiteKing / TEN - 1]);
				opcode = k->hasCheck(_whiteKing, board, _turn, '1');
			}
			
		}
		delete k;

		//returns to the first state
		int tempPos = (pos % HUNDRED) * HUNDRED + pos / HUNDRED;
		movePiece(board, tempPos, false);

		if (opcode != 4) //if the move is valid 
		{
			movePiece(board, pos, 0); //move the piece
		}

	}

	string opCodeString = to_string(opcode);
	return opCodeString;
}

/*
Function that turns a string to int (e4e2 --> 5452)
*/
int Comm::strToPos(std::string str)
{
	int pos = 0;
	for (int i = 0; i < str.size(); i++)
	{
		pos *= TEN;
		switch (str[i])
		{
		case 'a':
			pos += 1;
			break;
		case 'b':
			pos += 2;
			break;
		case 'c':
			pos += 3;
			break;
		case 'd':
			pos += 4;
			break;
		case 'e':
			pos += 5;
			break;
		case 'f':
			pos += 6;
			break;
		case 'g':
			pos += 7;
			break;
		case 'h':
			pos += 8;
			break;

		default:
			if (str[i] - '0' >= 1 && str[i] - '0' <= 8)
			{
				pos += (str[i] - '0');
			}
		
			break;
		}
		
	}
	return pos;
}
